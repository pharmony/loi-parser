# LOI Parser

This gem reads the `.loi` file format, parses its header and bitmap allowing you
to:
* retrieve the file's application, date, range, and version
* check if s serial number is in opposition
* grab all the serial numbers being marked in opposition

So far this gem only support "LOI" application files, not "dLOI" application
files.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'loi_parser'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install loi_parser

## Usage

```ruby
> loi = LoiParser::Reader.new('/path/to/your/file.loi')

# Check the file header:
> loi.header
 => {:length=>"0042", :application=>"LOI", :version=>"01", :date=>"20200213", :date_range=>"1019", :list_format=>"BTMP", :bitmap_size=>32000000}

# Checking a given serial number being in opposition
> loi.in_opposition?(99_999_999)
 => true
> loi.in_opposition?(12_345_678)
 => false

# Grabbing all the serial numbers being in opposition
> loi.serials_in_opposition
I, [2023-03-14T14:33:12.606494 #135]  INFO -- LoiParser: 0% Found: 0 (Elapsed: 0 ms)
I, [2023-03-14T14:33:12.610240 #135]  INFO -- LoiParser: 1% Found: 0 (Elapsed: 3 ms)
I, [2023-03-14T14:33:12.616639 #135]  INFO -- LoiParser: 2% Found: 0 (Elapsed: 10 ms)
I, [2023-03-14T14:33:12.621245 #135]  INFO -- LoiParser: 3% Found: 0 (Elapsed: 14 ms)
I, [2023-03-14T14:33:12.625052 #135]  INFO -- LoiParser: 4% Found: 0 (Elapsed: 18 ms)
...
I, [2023-03-14T14:33:13.116982 #135]  INFO -- LoiParser: 99% Found: 50 (Elapsed: 510 ms)
I, [2023-03-14T14:33:13.120837 #135]  INFO -- LoiParser: 100% Found: 50 (Elapsed: 514 ms)
I, [2023-03-14T14:33:13.130306 #135]  INFO -- LoiParser: Gathered 5113 serial number(s) in opposition in 523 ms.
```

**WARNING:** This gem doesn't have a _callback_ mechanism to pass found serial
numbers on the go. Instead it stores all the serial numbers found in an Array
which leads to a overflow failure depending on the machine's memory size.

Extracting all the serial numbers was an experiment we did in order to store
the serial numbers in a database, but we have abondonned this way since it is
very slow.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/pharmony/loi-parser.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
