# frozen_string_literal: true

require 'zip'

require 'loi_parser'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globaly on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    working_dir = File.join(ENV.fetch('PWD'), 'spec', 'fixtures')

    Zip::File.open(File.join(working_dir, '202002131019.loi.zip')) do |zip_file|
      zip_file.each do |file|
        destination = File.join(working_dir, file.name)

        zip_file.extract(file, destination) unless File.exist?(destination)
      end
    end
  end
end
