# frozen_string_literal: true

FIXTURES_PATH = File.join(ENV.fetch('PWD'), 'spec', 'fixtures')

RSpec.describe LoiParser::Reader do
  describe 'new' do
    context "when passing a path which doesn't exist" do
      let(:loi_path) { '/this/path/doesnt/exist' }

      it 'raises a RuntimeError' do
        expect { described_class.new(loi_path) }
          .to raise_error(RuntimeError)
      end
    end

    context 'when passing a path which is not an LOI file' do
      let(:loi_path) { File.join(FIXTURES_PATH, '202002131019.loi.zip') }

      it 'raises a LoiParser::Reader::InvalidLoiFile' do
        expect { described_class.new(loi_path) }
          .to raise_error(LoiParser::Reader::InvalidLoiFile)
      end
    end

    context 'when passing a path which is a LOI file' do
      let(:loi_path) { File.join(FIXTURES_PATH, '202002131019.loi') }
      let(:loi) { described_class.new(loi_path) }

      it 'parses the file header' do
        expect(loi.header).to be_a(Hash)
      end

      it 'retrieves the file header length name' do
        expect(loi.header[:length]).to eql('0042')
      end

      it 'retrieves the file application name' do
        expect(loi.header[:application]).to eql('LOI')
      end

      it 'retrieves the file version' do
        expect(loi.header[:version]).to eql('01')
      end

      it 'retrieves the file date' do
        expect(loi.header[:date]).to eql('20200213')
      end

      it 'retrieves the file date_range' do
        expect(loi.header[:date_range]).to eql('1019')
      end

      it 'retrieves the file list_format' do
        expect(loi.header[:list_format]).to eql('BTMP')
      end

      it 'retrieves the file bitmap_size' do
        expect(loi.header[:bitmap_size]).to be(32_000_000)
      end
    end
  end

  describe 'in_opposition?' do
    let(:loi_path) { File.join(FIXTURES_PATH, '202002131019.loi') }
    let(:loi) { described_class.new(loi_path) }

    context 'when passing a non numeric argument' do
      let(:serial_number) { 'abc123' }

      it 'raises an ArgumentError' do
        expect { loi.in_opposition?(serial_number) }
          .to raise_error(ArgumentError, 'serial_number must be an Integer')
      end
    end

    context 'when passing a serial number which is not in opposition' do
      let(:serial_number) { 12_345_678 }

      it 'is falsy' do
        expect(loi.in_opposition?(serial_number)).to be_falsy # rubocop:disable RSpec/PredicateMatcher
      end
    end

    context 'when passing a serial number which is in opposition' do
      let(:serial_number) { 99_999_999 }

      it 'is truthy' do
        expect(loi.in_opposition?(serial_number)).to be_truthy # rubocop:disable RSpec/PredicateMatcher
      end
    end
  end

  describe 'serials_in_opposition' do
    before(:all) do # rubocop:disable RSpec/BeforeAfterAll
      @result = described_class.new(
        File.join(FIXTURES_PATH, '202002131019.loi')
      ).serials_in_opposition
    end

    it 'returns 5113 serial numbers in opposition' do
      expect(@result.count).to be(5113) # rubocop:disable RSpec/InstanceVariable
    end

    it 'returns a list starting with the serial number 33541068' do
      expect(@result.min).to eq(33_541_068) # rubocop:disable RSpec/InstanceVariable
    end

    it 'returns a list ending with the serial number 256011368' do
      expect(@result.max).to eq(256_011_375) # rubocop:disable RSpec/InstanceVariable
    end

    it 'returns a list including the serial number 99999999' do
      expect(@result).to include(99_999_999) # rubocop:disable RSpec/InstanceVariable
    end
  end
end
