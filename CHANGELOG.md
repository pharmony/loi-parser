# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2023-03-14
### Added
- Configurable file.read length
- Thousands formater on the "founds" result

### Changed
- Reading 5 000 bytes by default in order to speed up the reading process
- Logging data

## [1.0.0] - 2023-03-13
### Added
- Initial import

[Unreleased]: https://gitlab.com/pharmony/loi-parser/compare/v1.1.0...master
[1.1.0]: https://gitlab.com/pharmony/loi-parser/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/hydrana/hydrana/tags/v1.0.0
