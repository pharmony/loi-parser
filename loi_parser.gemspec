# frozen_string_literal: true

require_relative 'lib/loi_parser/version'

Gem::Specification.new do |spec|
  spec.name          = 'loi_parser'
  spec.version       = LoiParser::VERSION
  spec.authors       = ['Pharmony']
  spec.email         = ['dev@pharmony.eu']

  spec.summary       = 'LOI (Liste d’Opposition Incrémentale) file reader'
  spec.description   = 'This gem parses the `.loi` file format and allows ' \
                       'to check if a given National Insurance Number card ' \
                       "serial number is marked as 'in opposition', or to " \
                       'extract all the serial numbers in opposition.'
  spec.homepage      = 'https://gitlab.com/pharmony/loi-parser'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.4.0')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added
  # into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:spec)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'logger'
end
