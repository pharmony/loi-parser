# frozen_string_literal: true

require 'logger'

require_relative 'loi_parser/configuration'
require_relative 'loi_parser/helpers'
require_relative 'loi_parser/reader'
require_relative 'loi_parser/version'

module LoiParser # rubocop:disable Style/Documentation
  class << self
    attr_writer :logger

    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
    end

    def logger
      @logger ||= Logger.new($stdout).tap do |log|
        log.progname = name
      end
    end
  end
end
