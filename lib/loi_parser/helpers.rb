# frozen_string_literal: true

module LoiParser
  #
  # This module contain various helper methods
  #
  module Helpers
    def self.elapsed_time(elapsed)
      if elapsed < 1
        "#{(elapsed * 1000).to_i} ms"
      elsif elapsed <= 60
        "#{elapsed.to_i} seconds"
      else
        min, sec = elapsed.divmod(60)

        "#{min} minutes #{sec.to_i} seconds"
      end
    end

    # 1000 #=> 1,000
    def self.formatted_thousands(number)
      number.to_s.gsub(/\B(?=(...)*\b)/, ',')
    end
  end
end
