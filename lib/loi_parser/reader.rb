# frozen_string_literal: true

module LoiParser
  #
  # Read about the Ruby's String#unpack method:
  # https://ruby-doc.org/3.0.4/String.html#method-i-unpack
  #
  # Read about the Ruby's IO#seek method:
  # https://ruby-doc.org/3.0.4/IO.html#method-i-seek
  #
  # Ruby Bitwise Operators
  # https://medium.com/rubycademy/ruby-bitwise-operators-da57763fa368
  #
  class Reader
    class InvalidLoiFile < StandardError; end

    attr_reader :header, :path

    HEADER_COLUMNS = [
      { field: :length, length: 4 },
      { field: :application, length: 20 },
      { field: :version, length: 2 },
      { field: :date, length: 8 },
      { field: :date_range, length: 4 },
      { field: :list_format, length: 4 },
      { field: :bitmap_size, length: 8 }
    ].freeze

    def initialize(path)
      @path = path

      open_file!

      @header = build_header
      @list_start_position = @file.pos
    end

    def in_opposition?(serial_number)
      unless serial_number.is_a?(Integer)
        raise ArgumentError, 'serial_number must be an Integer'
      end

      # Reading the bitmap list
      position = serial_number / 8

      # When position is out of bitmap range, the serial number is in opposition
      return true if position > header[:bitmap_size]

      # Moves to the serial number position from the bitmap
      @file.seek(position, IO::SEEK_CUR)

      # Read the byte as binary
      byte = @file.read(1).unpack1('B*')

      # Checks for opposition
      opposition = serial_in_opposition_from?(serial_number, byte)

      # Moves back to the initial position
      @file.seek(@list_start_position, IO::SEEK_SET)

      opposition
    end

    def serials_in_opposition
      # https://blog.dnsimple.com/2018/03/elapsed-time-with-ruby-the-right-way/
      @starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)

      serial_numbers = find_all_serial_numbers_in_opposition

      LoiParser.logger.info(
        "Gathered #{serial_numbers.size} serial number(s) " \
        "in opposition in #{elapsed_time_in_minutes}."
      )

      serial_numbers
    end

    private

    def serial_in_opposition_from?(serial_number, byte)
      byte.to_i(2) & (1 << (serial_number % 8)) != 0
    end

    def build_header
      header = read_header_from_file

      header[:application] = header[:application].strip
      header[:bitmap_size] = header[:bitmap_size].to_i

      raise InvalidLoiFile unless %w[LOI dLOI].include?(header[:application])

      header
    end

    def current_bytes_pos
      @file.pos - @list_start_position - 1
    end

    def elapsed_time_in_minutes
      Helpers.elapsed_time(
        Process.clock_gettime(Process::CLOCK_MONOTONIC) - @starting
      )
    end

    def extract_serial_mumbers_from(bytes)
      # Splits bytes in groups of 8 chars
      splited_bytes = bytes.scan(/.{1,8}/)

      splited_bytes.each.with_index(1).flat_map do |byte, bindex|
        # Calculates the position in file for the current byte
        current_byte_pos = current_bytes_pos - (splited_bytes.size - bindex)

        # Go through the 8 bites looking for '1'
        byte.each_char.with_index(1).map do |bit, index|
          next if bit == '0'

          # Calculates the serial number from the position in the file plus the
          # offset of the bit
          ((current_byte_pos * 8) + (8 - index))
        end
      end.compact
    end

    def find_all_serial_numbers_in_opposition
      serial_numbers = []

      until (readed = @file.read(LoiParser.configuration.read_length)).nil?
        bytes = readed.unpack1('B*')

        print_percentage(serial_numbers.size)

        next unless bytes.include?('1')

        serial_numbers |= extract_serial_mumbers_from(bytes)
      end

      serial_numbers
    end

    def open_file!
      unless File.exist?(@path)
        raise "#{self.class} ERROR: #{@path} doesn't exist."
      end

      @file = File.open(@path)
    end

    def print_percentage(found)
      percentage = (@file.pos * 100) / header[:bitmap_size]

      return if percentage == @current_percentage

      LoiParser.logger.info(
        "#{percentage}% Found: #{Helpers.formatted_thousands(found)} (" \
        "Elapsed: #{elapsed_time_in_minutes})"
      )

      @current_percentage = percentage
    end

    def read_header_from_file
      HEADER_COLUMNS.each_with_object({}) do |column, acc|
        acc[column[:field]] = @file.read(column[:length]).unpack1('M')
      end
    end
  end
end
