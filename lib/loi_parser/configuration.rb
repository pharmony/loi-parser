# frozen_string_literal: true

module LoiParser
  #
  # LoiParser gem configuration class
  #
  class Configuration
    # file.read length to be used to read the LOI file
    attr_accessor :read_length

    def initialize
      @read_length = 5000
    end
  end
end
